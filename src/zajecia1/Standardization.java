package zajecia1;

public class Standardization {
    static double[] z = new double[100];
    static double average;
    static double data[] = DataReading.getData();

    static public void getResult() {
        double sum = 0;
        for (int i = 0; i < data.length; i++) sum += data[i];
        average = sum / data.length;

        double temp = 0;
        for (double a : data)
            temp += (a - average) * (a - average);
        double variance = temp / (data.length - 1);
        double sigma = Math.sqrt(variance);

        for (int i = 0; i < data.length; i++) {
            z[i] = (data[i] - average) / sigma;
            System.out.println(i + 1 + ".  " + data[i] + "    " + z[i]);
        }
    }

    public void CalcSumZ() {
        double sumZ = 0;
        for (double a : z) {
            sumZ += a;
        }
        System.out.println("Suma Z wynosi:  " + sumZ);
    }
}
