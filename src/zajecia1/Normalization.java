package zajecia1;

public class Normalization {
    static double[] z = new double[100];

    static double data[] = DataReading.getData();
    static double min = data[0];
    static double max = data[0];

    static public void getResult() {
        for (int i = 0; i < data.length; i++) {
            if (data[i] <= min) {
                min = data[i];
            }
            if (data[i] >= max) {
                max = data[i];
            }
        }
        System.out.println("min: " + min);
        System.out.println("max: " + max);

        for (int i = 0; i < data.length; i++) {
            z[i] = ((data[i] - min) / (max - min));
            System.out.print(i + 1 + ".   " + data[i] + "   ");
            System.out.println(z[i]);
        }
    }
}
