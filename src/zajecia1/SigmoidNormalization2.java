package zajecia1;

public class SigmoidNormalization2 {
    static double[] z = new double[100];
    static double[] dane = DataReading.getData();

    static void getResult() {
        for (int i = 0; i < dane.length; i++) {
            z[i] = (Math.exp(dane[i]) - Math.exp(-dane[i])) / (Math.exp(dane[i]) + Math.exp(-dane[i]));
            System.out.println(i + 1 + ".  " + z[i]);
        }
    }

}
