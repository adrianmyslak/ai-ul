package zajecia1;

public class SigmoidNormalization1 {
    static double[] z = new double[100];
    static double[] data = DataReading.getData();

    static void getResult() {
        for (int i = 0; i < data.length; i++) {
            z[i] = 1 / (1 + Math.exp(-data[i]));
            System.out.println(i + 1 + ".  " + z[i]);
        }
    }
}
