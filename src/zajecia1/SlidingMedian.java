package zajecia1;

import java.util.Arrays;

public class SlidingMedian {
    static double[] data = DataReading.getData();
    static double[] z = new double[data.length];

    static void getResult(double m) {
        double[] table = new double[(int) m];
        for (int i = (int) (m / 2); i < data.length - (m / 2); i++) {
            if (m == 3 || m == 5 || m == 7) {
                for (int j = 0; j < m; j++)
                    table[j] = data[i - (int) (m / 2) + j];
            } else return;
            Arrays.sort(table);
            System.out.println(i + 1 + "   " + table[(int) m / 2]);
        }
    }
}
