package zajecia1;

import java.util.Arrays;

public class SlidingCutAverage {
    static double[] data = DataReading.getData();

    static void getResult(double m) {
        double[] table = new double[(int) m];

        if (m == 5 || m == 7) {
            for (int i = (int) (m / 2); i < data.length - (m / 2); i++)  //ograniczenie index -2 +2
            {
                for (int j = 0; j < m; j++) {
                    table[j] = data[i - (int) (m / 2) + j];
                }
                Arrays.sort(table);
                double sum = 0;
                double average = 0;
                for (int k = 0; k < table.length; k++) {
                    if (k > (m / 2) - 2 && k <= (m / 2) + 1) sum += table[k];
                }
                average = sum / 3;
                System.out.println(i + 1 + "   " + average);
            }
        } else System.out.println("Wprowadzono niepoprawny paramentr m");
    }
}
