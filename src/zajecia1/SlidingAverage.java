package zajecia1;

public class SlidingAverage {
    static double[] data = DataReading.getData();
    static double[] z = new double[data.length];

    static void getResult(double m) {
        for (int i = (int) (m / 2); i < data.length - (m / 2); i++) {
            if (m == 3) z[i] = ((1 / m) * (data[i - 1] + data[i] + data[i + 1]));
            else if (m == 5) z[i] = ((1 / m) * (data[i - 2] + data[i - 1] + data[i] + data[i + 1] + data[i + 2]));
            else if (m == 7)
                z[i] = ((1 / m) * (data[i - 3] + data[i - 2] + data[i - 1] + data[i] + data[i + 1] + data[i + 2] + data[i + 3]));
            else return;
            System.out.println(i + 1 + ".  " + z[i]);
        }
    }
}
